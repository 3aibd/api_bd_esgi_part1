//------------------------------------------------------------------------------
// HEADERS : create_table.h
//----------------------------------------------------------------------------------

#ifndef API_BD_CREATE_TABLE_H
#define API_BD_CREATE_TABLE_H

/*Fonction qui demande ce que l'utilisateur souhaite faire*/
void askUserAction();

/*Fonction qui crée le base de donnée et retourne un entier flag*/
int createTableInDataBase(int positionCursor, char *author, int id);

/*Fonction qui crée une table dans la base de donnée et retourne un entier flag*/
int createDataBase();

int yamlParser(char *chaine);

int renameFile(char *concName, char *nameData);

int deleteFileTable(char *nameFile);

int deleteTableInDataBase(int pos);

int yamlSearchPositionCursor(char *nameTable, int positionCursor);

int insertAttributeInTable(FILE* file,FILE* fileTable);

FILE* createTAbleInBase(char* nameTable,char* author,char* name,char* type,char* autoIncrement,char* primaryKey,char* nullable);
#endif //API_BD_CREATE_TABLE_H
