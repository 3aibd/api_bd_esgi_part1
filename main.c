#include <stdio.h>
#include <stdlib.h>
#include "Headers/create_yaml.h"
#include <string.h>

int main( )
{
    /*************************************************
    *Variables globale
    */
    int choice = 0;
    int dataBaseIsCreate = 0;
    int tableIsCreate = 1;
    int positionCursor=0;
    int positionCursorCreate=0;

    char concTableNameInDataBase[20] = "tableName: ";
    char concNameFile[20] = "..\\FichierYaml\\";
    char tableName[20] = "";

    char dataBaseName[20] = "";
    char concNameBaseFile[20] = "";

    char author[20] = "manitra";
    int requeteCreate = 0;
    int rec = 0;
    FILE* file = NULL;
    /***************************************************
    *FIN Variables globale
    */


    /******************************************************************************************************
     * PROGRAMME PRINCIPAL
     ******************************************************************************************************/
    //Demande du choix de l'utilisateur---------------------------------

    do
    {
        askUserAction();
        scanf("%d",&choice);

        printf("choix : %d \n",choice);

        //UTILISATEUR CREER UNE BASE DE DONNEE-----------------------------------
        if (choice == 1)
        {
            positionCursor = createDataBase();
            dataBaseIsCreate = dataBaseIsCreate+ 1;

            //Si une base de donnée est crée, création d'une table
            do{
                tableIsCreate = createTableInDataBase(positionCursor, author, tableIsCreate) + tableIsCreate;
                //Utilisateur ajoute une table dans la base de donnée------------------------------------------
                //On demande a l'utilisateur si il veut creer d'autre table
                printf("Voulez vous creer une autre table ?\n");
                printf("[1]Oui ou [0]Non");
                scanf("%d", &requeteCreate);
            }
            while(requeteCreate != 0);

        }

        //Utilisation d'une base de donnée existante-----------------------------
        if(choice==2 && dataBaseIsCreate>0)
        {
            tableIsCreate = createTableInDataBase(positionCursor, author, tableIsCreate) + tableIsCreate;

        }
        else if(choice==2 && dataBaseIsCreate<0)
        {
            printf("Aucune base n'est encore cree, Veuillez creer une BASE en faisant le choix 1");
        }



        //Utilisateur ajoute une table---------------------------------------------
        if(choice==3 && dataBaseIsCreate>0 )
        {
            do
            {
                tableIsCreate = createTableInDataBase(positionCursor, author, tableIsCreate) + tableIsCreate;

                //UTILISATEUR CREER UNE TABLE
                //On demande a l'utilisateur si il veut creer d'autre table
                printf("Voulez vous creer une autre table ?\n");
                printf("[1]Oui ou [0]Non");
                scanf("%d  \n", &requeteCreate);
            }
            while (requeteCreate != 0);
        }


        //UTILISATEUR SUPPRIME UNE TABLE-------------------------------------------
        if(choice==4)
        {
            //Concaténation pour formaliser par rapport au yaml de BASE DE DONNEE
            printf("Donner le nom de la table a supprimer:\n");
            scanf("%s", tableName);
            strcat(concTableNameInDataBase, tableName);

            //On recherche la positionCursor de la table dans le yaml
            positionCursor = yamlParser(concTableNameInDataBase);

            //on affiche la positionCursor
            printf("positionCursor = %d \n",positionCursor);

            //On Concataine le file
            renameFile(concNameFile,tableName);
            //On supprime le file
            deleteFileTable(concNameFile);

            //Suppression dans le fichier de la base de donnée
            deleteTableInDataBase(positionCursor);
        }

        //UTILISATEUR DETRUIT LA BASE DE DONNEE---------------------------------------
        if(choice==5)
        {
            printf("Donner le nom de la table a detruire:\n");
            scanf("%s", dataBaseName);
            //On Concataine le file
            renameFile(concNameBaseFile,dataBaseName);
            //On supprime le file
            deleteFileTable(concNameBaseFile);
        }
        printf("Voulez vous refaire des requete : [1]Oui [0]Non \n");
        scanf("%d", &rec);
    }
    //----------------------------------------------------------------------------------------
    while(rec!=0);

    //Fermeture du fichier
    fclose(file);

    return 0;
}




