//----------------------------------------------------------------------:
// Sources : create_table.c
//----------------------------------------------------------------------

//------------------------------------------------------------
//LIBRAIRIE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Headers/create_yaml.h"
//------------------------------------------------------------
#define TAILLE_MAX 1000
//*****************************************************************
//Fonction : Demande action
//*****************************************************************
void askUserAction()
{

    printf("Que voulez vous faire\n");
    printf("[1] creer une base de donnee\n");
    printf("[2] utiliser une base de donnee existante\n");
    printf("[3] ajouter une table\n");
    printf("[4] supprimer une table\n");
    printf("[5] detruire la base de donnee\n");
    printf("[0] QUITTER\n");
    printf("Faite un choix :\n");

}

//*****************************************************************
//Fonction création de la Base
//*****************************R************************************
int createDataBase()
{
    //----------------------------------------
    // Variable Local
    FILE *fileDataBase;

    char conc_nom_de_la_base[TAILLE_MAX] = "";
    char nom_de_la_base[20] = "";
    int positionCursor=0;
    //----------------------------------------

    // On lit et on écrit dans le fichier
    printf("Donner le nom de votre BD:\n");
    scanf("%s", nom_de_la_base);

    //Appel de la fonction de renommage du fichier pour la création
    renameFile(conc_nom_de_la_base,nom_de_la_base);

    //Ouverture du fichier
    fileDataBase=fopen(conc_nom_de_la_base, "w");

    //Si le fichier est bien ouvert
    if(fileDataBase!=NULL)
    {
        //affiche l'positionCursor
        printf("%d \n",positionCursor);

        //ecriture dans le fichier.yaml
        fprintf(fileDataBase, "databaseChangeLog:\n");
        fprintf(fileDataBase, " - preConditions:\n");
        fprintf(fileDataBase, "   - runningAs:\n");
        fprintf(fileDataBase, "\t\t\t username: ");
        fprintf(fileDataBase, nom_de_la_base);
        fprintf(fileDataBase, "\n\n");
        positionCursor = ftell(fileDataBase);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("Impossible d'ouvrir le fichier ");
    }
    return positionCursor;
}


//*****************************************************************
//Fonction création table_in_data_base
//*****************************************************************
//A Modifier
int createTableInDataBase(int positionCursor, char *author, int id)
{
    //----------------------------------------------------------------------
    //VARIABLE LOCAL
    FILE* file;
    FILE* fileTable;

    char concNameTableInDataBase[TAILLE_MAX] = "";
    char nameTable[20] = "";
    char nameDataBase[20] = "";
    int tableIsCreate=0;
    char name[20] = "";
    char type[20] = "";
    char autoIncrement[20] = "";
    char primaryKey[20]="";
    char nullable[20]="";
    int choix=0;
    //---------------------------------------------------------------------


    printf("Donner le nom de la BASE ou vous souhaiter creer la table:\n");
    scanf("%s", nameDataBase);

    printf("Donner le nom de votre Table:\n");
    scanf("%s", nameTable);

    //Concatenation du nom de file yaml pour pouvoir bien les classer
    renameFile(concNameTableInDataBase, nameDataBase);

    //Création et ouverturechainedu deuxième file
    file = fopen(concNameTableInDataBase, "w");
    if(file!=NULL)
    {
        //Place le curseur au debut du fichier
        fseek(file,positionCursor,SEEK_SET);

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, "- changeSet:\n");
        fprintf(file, "\t\tid: ");
        fprintf(file, "%d",id);
        fprintf(file, "\n");
        fprintf(file, "\t\tauthor: ");
        fprintf(file, author);
        fprintf(file, "\n\t\tchanges:");
        fprintf(file, "\n\t\t\t- createTable: ");
        fprintf(file, "\n\t\t\t\ttableName: ");
        fprintf(file, nameTable);
        fprintf(file, "\n\t\t\t\tcolumns:\n");
        fprintf(file, "\t\t\t\t\t- column:\n");
        fprintf(file, "\t\t\t\t\t\t\tname: ");

        printf("name column:");
        scanf("%s",name);
        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, name);

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, "\n\t\t\t\t\t\t\ttype: ");

        printf("type:");
        scanf("%s",type);

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, type);

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, "\n");
        fprintf(file, "\t\t\t\t\t\t\tautoIncrement: ");

        printf("autoIncrement:");
        scanf("%s",autoIncrement);
        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, autoIncrement);

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, "\n\t\t\t\t\t\t\tconstraints: ");
        fprintf(file, "\n\t\t\t\t\t\t\t\tprimaryKey: ");

        printf("primaryKey:");
        scanf("%s",primaryKey);

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        fprintf(file, primaryKey);
        fprintf(file, "\n");

        //-----------------------------------------------------
        //Ecriture dans le file de base de donnée
        printf("nullable:");
        scanf("%s",nullable);
        fprintf(file, "\t\t\t\t\t\t\t\tnullable: ");
        fprintf(file, nullable);

        //--------------------------------------------------------
        printf("Voulez-vous creer d'autre attribut ?\n");
        printf("[1]Oui\n");
        printf("[2]Non\n");
        scanf("%d",&choix);

        //Insertion des donnée dans le fichier de la Table
        fileTable = createTAbleInBase(nameTable,author,name,type,autoIncrement,primaryKey,nullable);

        while(choix==1)
        {
            choix = insertAttributeInTable(file,fileTable);

        }

        fprintf(file,"\n\n");
        fprintf(fileTable,"\n\n");

        tableIsCreate = tableIsCreate + 1;

        fclose(file);
        fclose(fileTable);
    }
    else
    {
        printf("Impossible d'ouvrir le fichier ");
    }

    return tableIsCreate;


}

int insertAttributeInTable(FILE* file,FILE* fileTable)
{
    char name[20] = "";
    char type[20] = "";
    int myChoice=0;

    //-----------------------------------------------------
    //Ecriture dans le file de base de donnée
    fprintf(file, "\n\t\t\t\t\t- column:\n");
    fprintf(file, "\t\t\t\t\t\t\tname: ");
    fprintf(fileTable, "\n\t\t\t\t\t- column:\n");
    fprintf(fileTable, "\t\t\t\t\t\t\tname: ");
    printf("name column:");


    scanf("%s", name);

    fprintf(file, name);
    fprintf(fileTable, name);

    fprintf(file, "\n\t\t\t\t\t\t\ttype: ");
    fprintf(fileTable, "\n\t\t\t\t\t\t\ttype: ");

    printf("type:");
    scanf("%s", type);

    //Ecriture dans le file de base de donnée
    fprintf(file, type);
    fprintf(fileTable, type);


    printf("Voulez-vous creer d'autre attribut ?\n");
    printf("[1]Oui\n");
    printf("[2]Non\n");
    scanf("%d",&myChoice);

    return myChoice;
}
FILE* createTAbleInBase(char* nameTable,char* author,char* name,char* type,char* autoIncrement,char* primaryKey,char* nullable)
{
    FILE* fileTable;
    #define TAILLE_MAX 1000
    char concNameTable[TAILLE_MAX] = "";

    //Concatenation du nom de file yaml pour pouvoir bien les classer
    renameFile(concNameTable, nameTable);

    //Création et ouverturechainedu deuxième file
    fileTable = fopen(concNameTable, "w");
    if(fileTable!=NULL)
    {
//-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, "- changeSet:\n");
        fprintf(fileTable, "\t\tid: 1\n");
        fprintf(fileTable, "\t\tauthor: ");
        fprintf(fileTable, author);
        fprintf(fileTable, "\n\t\tchanges:");
        fprintf(fileTable, "\n\t\t\t- createTable: ");
        fprintf(fileTable, "\n\t\t\t\ttableName: ");
        fprintf(fileTable, nameTable);
        fprintf(fileTable, "\n\t\t\t\tcolumns:\n");
        fprintf(fileTable, "\t\t\t\t\t- column:\n");
        fprintf(fileTable, "\t\t\t\t\t\t\tname: ");


        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, name);

        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, "\n\t\t\t\t\t\t\ttype: ");

        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, type);

        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, "\n");
        fprintf(fileTable, "\t\t\t\t\t\t\tautoIncrement: ");

        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, autoIncrement);

        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, "\n\t\t\t\t\t\t\tconstraints: ");
        fprintf(fileTable, "\n\t\t\t\t\t\t\t\tprimaryKey: ");


        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, primaryKey);
        fprintf(fileTable, "\n");


        //-----------------------------------------------------
        //Ecriture dans le file de la table
        fprintf(fileTable, "\t\t\t\t\t\t\t\tnullable: ");
        fprintf(fileTable, nullable);
    }
    else
    {
        printf("Impossible d'ouvrir le fichier ");
    }
 return fileTable;
}



//*****************************************************************
//Fonction yamlParser
//*****************************************************************
int yamlParser(char *chaine)
{
    FILE *fileTable;
    char concNameTable[TAILLE_MAX] = "";
    char name[TAILLE_MAX] = "";

    char nameTable[20] = "";

    int i=0;
    int positionCursor = 0;
    int pos=0;
    char *endChaine;


    printf("Donner le nom de la table a chercher:\n");
    scanf("%s", nameTable);

    //Concatenation du nom de fichier yaml pour pouvoir bien les classer
    renameFile(concNameTable, nameTable);

    //Création et ouverture chaine du deuxième fichier
    fileTable = fopen(concNameTable, "r");

    //tant que l'on arrive pas a la fin du fichier
    if(fileTable!=NULL)
    {
        //Lecture dans le fichier et stock dans une chaine de caractère=>name
        while (fgets(name, TAILLE_MAX,fileTable) != NULL)
        {
            //Place le curseur au debut du fichier
            fseek(fileTable,i,SEEK_SET);
            //Recupère son positionCursor
            positionCursor = (fileTable,i);
            //affiche l'positionCursor
            printf("%d \n",positionCursor);
            printf("%s \n", name); // On affiche la chaîne qu'on vient de lire
            printf("\n");

            // On cherche la première occurrence de "chaine" dans "name" :
            //On compare dans fichier
            endChaine = strstr(name, chaine);
            if (endChaine != NULL)
            {
                printf("TROUVE =>%s \n\n", endChaine);
                pos=positionCursor;
                break;
            }
            i++;
        }
        fclose(fileTable);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("Impossible d'ouvrir le fichier ");
    }

    return pos;
}

int yamlSearchPositionCursor(char *nameTable, int positionCursor)
{

    FILE *fileTable;
    char name[TAILLE_MAX] = "";
    int i=0;
    int positionEnd=0;
    char endChaine[TAILLE_MAX] = "- changeSet:";
    char *foundChaine;
    int tmpPos=0;
    //Création et ouverture chaine du deuxième fichier
    fileTable = fopen(nameTable, "r");

    //tant que l'on arrive pas a la fin du fichier
    if(fileTable!=NULL)
    {
        //Lecture dans le fichier et stock dans une chaine de caractère=>name
        while (fgets(name, TAILLE_MAX,fileTable) != NULL)
        {
            //Place le curseur à la position de la table dans le fichier
            fseek(fileTable,i,positionCursor);
            //Recupère la position
            tmpPos = (fileTable,i);
            //affiche la position du Cursor
            printf("%d \n",tmpPos);
            printf("%s \n", name); // On affiche la chaîne qu'on vient de lire
            printf("\n");

            // On cherche la première occurrence de "chaine" dans "name" :
            foundChaine = strstr(name, endChaine);
            if (foundChaine != NULL)
            {
                printf("TROUVE =>%s \n\n", foundChaine);
                positionEnd=tmpPos;
                break;
            }
            i++;
        }
        fclose(fileTable);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("Impossible d'ouvrir le fichier :");
        printf("%s \n",nameTable);
    }

    return positionEnd;
}



//*************************************************************
//Fonction de renommage d'un fichier yaml
//*************************************************************
int renameFile(char *concName, char *nameData)
{

    strcat(concName, "../FichierYaml/");
    strcat(concName, nameData);
    strcat(concName, ".yaml");

    printf("\n%s\n",concName);
    return 1;
}



//**************************************************************************
//Fonction qui detruit le fichier de la TABLE
//**************************************************************************
int deleteFileTable(char *nameFile)
{

    int suppression = 0;

    //renameFile(conc_nom_de_la_table, nameFile);
    suppression = remove(nameFile);

    return suppression;
}


//**************************************************************************
//Fonction qui detruit le fichier de la TABLE et dans le fichier de la BASE
//**************************************************************************
int deleteTableInDataBase(int pos)
{

    FILE *file;

    char conc_chemin_nom_base[TAILLE_MAX] = "";
    char conc_nom_de_la_base[TAILLE_MAX] = "";
    int position_fin;
    int positionCursor = 0;
    char nom_de_la_base[20] = "";
    int i = 0;
    printf("Donner le nom de votre BD:\n");
    scanf("%s", nom_de_la_base);

    //Concatenation du nom de file yaml pour pouvoir le supprimer
    renameFile(conc_chemin_nom_base, nom_de_la_base);

    position_fin = yamlSearchPositionCursor(conc_nom_de_la_base,pos);

    //Ouverture du file
    file=fopen(conc_nom_de_la_base, "w");
    printf("Chemin du fichier :\n %s \n",conc_nom_de_la_base);

    if(file!=NULL)
    {
        //Lecture dans le fichier et stock dans une chaine de caractère=>name
        while (i<position_fin)
        {
            //Place le curseur
            fseek(file,i,pos);

            positionCursor = (file,i);
            //affiche l'positionCursor
            printf("%d \n",positionCursor);
            fprintf(file,"");
            i++;
        }
        fclose(file);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("Impossible d'ouvrir le fichier :");
        printf("%s \n",conc_chemin_nom_base);
    }

    return pos;

}


//*****************************************************************************************************************************************************
// FIN PARTIE STOCKAGE ET MODIFICATION DANS YAML
//*****************************************************************************************************************************************************
